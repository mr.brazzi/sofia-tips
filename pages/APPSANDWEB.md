# Apps y Sitios web de utilidad

## Transporte

Para moverte por bulgaria hay una serie de apps que te van a facilitar **MUCHO** la vida. Asi que en cuanto leas esto, si no las tienes, instalatelas ya.

### Moovit ([Android](https://play.google.com/store/apps/details?id=com.tranzmate) | [iOS](https://apps.apple.com/us/app/moovit-train-bus-times/id498477945))

```
La mejor para el transporte publico, pero no para caminar ni andar en bicicleta o carro
```

Sin duda esta es la mas importante de todas. De las que hemos probado es la que mejor sincronizacion tiene con los GPSs del transporte de Sofia. Es tan sencilla como buscar tu destino al estilo Google Maps, buscar rutas, y escoger la que prefieras. Te mostrara una serie de opciones, con el tiempo de duracion del recorrido, el tiempo de espera para el bus o lo que sea que estes esperando, y al escoger tu opcion favorita, veras un seguimiento en tiempo real de todo tu recorrido. Estamos hablando de que veras por cual parada vas, notificaciones de cuando bajarte, cuanto falta para que llegue el metro, y mucho mas.
Como nada es perfecto, tiene tambien algunos problemillas. Por ejemplo, a veces tiene problemas con el compas. Ademas, por alguna razon no le gusta que camines, asi que podras encontrarte en una situacion donde caminar un poco suele llevarte a una mejor ruta que el no te muestra. Tambien, en el tiempo de duracion total del recorrido, no tiene en cuenta el tiempo intermedio de intercambio entre dos rutas, por lo que te muestra siempre un tiempo menor del que realmente demoraras y por tanto, el ordenamiento por "conveniencia" no siempre es el mejor.

### Google Maps ([Android](https://play.google.com/store/apps/details?id=com.google.android.apps.maps) | [iOS](https://apps.apple.com/us/app/google-maps-transit-food/id585027354))

Google Maps es sin duda de las mejores aplicaciones de su tipo (por no decir la mejor), pero dado a que no ha logrado obtener acceso al sistema publico de transporte de Bulgaria, no tiene muy buena sincronizacion con los GPSs y las rutas. Sin embargo, en cuanto al mapa, caminar, bici o carro, no tiene rival. Ademas, es el mejor medio para encontrar lugares de interes en la ciudad, desde bares y restaurantes, hasta lugares de interes turistico. Ah, y es la mejor manera de saber por cual lado del metro tienes que salir.

### EasyWay ([Android](https://play.google.com/store/apps/details?id=com.eway))

Esta app no la vas a usar tanto, pero te puede ser muy util. En ella podras encontrar un mapa y la descripcion de todas las rutas de transporte publico de la ciudad. Asi que si deseas saber ese bus raro que pasa por tu casa pa donde coge, o cuanta vuelta da, o de donde sale, o las rutas de los bus nocturnos, esta es tu mejor opcion. No la he encontrado para iOS, pero para ustedes, amantes de Apple, puede entrar al incomodo sitio del transporte publico de Sofia y disfrutar de su [mapa](https://www.sofiatraffic.bg/interactivecard/) "*interactivo*" :D.